<?php

namespace AutoposterBundle\Validator\Constraints;

use AutoposterBundle\Entity\Autopost;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class HashValidator extends ConstraintValidator
{
    /**
     * @var string
     */
    protected $secret;

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret(string $secret)
    {
        $this->secret = $secret;
    }

    /**
     * @param Autopost   $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Hash) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Hash');
        }

        if (!$value instanceof Autopost) {
            throw new UnexpectedTypeException($value, __NAMESPACE__.'\Autopost');
        }

        $generatedHash = md5(
            strtoupper($value->getCampaignCode()) .
            $value->getPhoneNumber() .
            $value->getDate()->format(\DateTime::ATOM) .
            $this->getSecret()
        );

        if ($generatedHash != $value->getHash()) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ hash }}', $value->getHash())
                ->addViolation();
        }
    }
}