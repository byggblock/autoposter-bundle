<?php

namespace AutoposterBundle\Service;

use AutoposterBundle\Entity\Autopost;
use AutoposterBundle\Entity\Revoke;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;

class AutopostSaverService implements AutopostSaverInterface
{
    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var string
     */
    protected $tableName        = '';

    /**
     * @var string
     */
    protected $tableRevokeName  = '';

    /**
     * @var array
     */
    protected $defaultTableMapping = [
        'campaign_code'     => 'campaign_code',
        'campaign_detail'   => 'campaign_detail',
        'phone_number'      => 'phone_number',
        'date'              => 'date',
        'hash'              => 'hash',
        'type'              => 'type',
        'custom_fields'     => 'custom_fields',
        'revoke_date'       => 'revoke_date',
        'id'                => 'id',
    ];

    /**
     * @var array
     */
    protected $defaultTableRevokeMapping = [
        'hash'              => 'hash',
        'reason'            => 'reason',
        'date'              => 'date',
        'autopost_id'       => 'autopost_id',
    ];

    /**
     * @var array
     */
    protected $tableMapping = [ ];

    /**
     * @var array
     */
    protected $revokeMapping = [ ];

    /**
     * AutopostSaverService constructor.
     *
     * @param Registry  $doctrine
     * @param string    $tableName
     * @param array     $mapping
     */
    public function __construct(Registry $doctrine, $tableName, $mapping, $tableRevokeName, $revokeMapping)
    {
        $this->doctrine             = $doctrine;
        $this->tableName            = $tableName;
        $this->tableRevokeName      = $tableRevokeName;
        $this->tableMapping         = array_merge($this->defaultTableMapping, $mapping);
        $this->tableRevokeMapping   = array_merge($this->defaultTableRevokeMapping, $revokeMapping);
    }

    /**
     * Save the Autopost object to the database.
     *
     * @param Autopost $record
     */
    public function save(Autopost &$record)
    {
        if ($this->tableMapping == $this->defaultTableMapping && $this->tableName == 'autopost') {
            $em = $this->doctrine->getEntityManager();
            $em->persist($record);
            $em->flush($record);

            return;
        }

        $data = [
            $this->tableMapping['campaign_code']   => $record->getCampaignCode(),
            $this->tableMapping['campaign_detail'] => $record->getCampaignDetail(),
            $this->tableMapping['phone_number']    => $record->getPhoneNumber(),
            $this->tableMapping['date']            => $record->getDate()->format(\DateTime::ATOM),
            $this->tableMapping['hash']            => $record->getHash(),
            $this->tableMapping['type']            => $record->getType(),

            // if the database doesnt support json fields, perhaps use different storage type.
            $this->tableMapping['custom_fields']   => \json_encode($record->getCustomFields()),
        ];

        /** @var Connection $connection */
        $connection = $this->doctrine->getConnection();
        $connection->insert($this->tableName, $data);

        $record->setId($connection->lastInsertId());
    }

    /**
     * Revoke the Autopost object to the database.
     *
     * @param Revoke $record
     * @return bool|string true or the error message
     */
    public function revoke(Revoke &$record)
    {
        if ($this->tableMapping == $this->defaultTableMapping && $this->tableName == 'autopost') {
            $em             = $this->doctrine->getEntityManager();
            $repository     = $em->getRepository(Autopost::class);
            /** @var Autopost|null $autopostObject */
            $autopostObject = $repository->findOneByHash($record->getHash());

            if (! $autopostObject) {
                return 'Record not found';
            }

            $autopostObject->setRevokeDate($record->getDate());
            $record->setAutopost($autopostObject);
            $em->persist($record);
            $em->flush($record);
            $em->flush($autopostObject);

            return true;
        }

        /** @var Connection $connection */
        $connection = $this->doctrine->getConnection();
        $sql        = sprintf('select * from %s where %s = :hash', $this->tableName, $this->tableMapping['hash']);
        $records    = $connection->fetchAll($sql, [ 'hash' => $record->getHash() ] );
        $recordPost = current($records);
        $count      = $connection->update(
            $this->tableName,
            [ $this->defaultTableMapping['revoke_date'] => (new \DateTime())->format('Y-m-d H:i:s') ],
            [ $this->defaultTableMapping['id']          => $recordPost[$this->tableMapping['id']] ]
        );

        $revokeData = [
            $this->tableRevokeMapping['hash']         => $record->getHash(),
            $this->tableRevokeMapping['reason']       => $record->getReason(),
            $this->tableRevokeMapping['date']         => $record->getDate()->format('Y-m-d H:i:s'),
            $this->tableRevokeMapping['autopost_id']  => $recordPost[$this->tableMapping['id']],
        ];

        $connection->insert($this->tableRevokeName, $revokeData);

        if (! $count) {
            return 'Record not found';
        }

        return true;
    }
}