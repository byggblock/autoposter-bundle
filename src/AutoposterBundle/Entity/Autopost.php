<?php

namespace AutoposterBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Autopost
 * @package AutoposterBundle\Entity
 *
 * @ORM\Table(name="autopost")
 * @ORM\Entity
 */
class Autopost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("campaigncode")
     *
     * @ORM\Column(name="campaign_code", type="string", length=20, nullable=true)
     */
    protected $campaignCode;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("campaigndetail")
     *
     * @ORM\Column(name="campaign_detail", type="string", length=255, nullable=true)
     */
    protected $campaignDetail;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("phonenumber")
     *
     * @ORM\Column(name="phone_number", type="string", length=50, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("timestamp")
     *
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("hash")
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    protected $hash;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("type")
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @JMS\Type("array")
     * @JMS\SerializedName("customfields")
     *
     * @ORM\Column(name="custom_fields", type="json_array", nullable=true)
     */
    protected $customFields;

    /**
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("revoke_date")
     *
     * @ORM\Column(name="revoke_date", type="datetime", nullable=true)
     */
    protected $revokeDate;

    /**
     * @ORM\OneToMany(targetEntity="Revoke", mappedBy="autopost")
     */
    protected $updates;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCampaignDetail()
    {
        return $this->campaignDetail;
    }

    /**
     * @param mixed $campaignDetail
     */
    public function setCampaignDetail($campaignDetail)
    {
        $this->campaignDetail = $campaignDetail;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCampaignCode()
    {
        return $this->campaignCode;
    }

    /**
     * @param mixed $campaignCode
     */
    public function setCampaignCode($campaignCode)
    {
        $this->campaignCode = $campaignCode;
    }

    /**
     * @return mixed
     */
    public function getCustomFields()
    {
        return $this->customFields;
    }

    /**
     * @param mixed $customFields
     */
    public function setCustomFields($customFields)
    {
        $this->customFields = $customFields;
    }

    /**
     * @return mixed
     */
    public function getRevokeDate()
    {
        return $this->revokeDate;
    }

    /**
     * @param mixed $revokeDate
     */
    public function setRevokeDate($revokeDate)
    {
        $this->revokeDate = $revokeDate;
    }
}