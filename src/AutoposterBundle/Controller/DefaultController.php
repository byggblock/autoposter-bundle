<?php

namespace AutoposterBundle\Controller;

use AutoposterBundle\Entity\Autopost;
use AutoposterBundle\Entity\Revoke;
use AutoposterBundle\Service\AutopostSaverInterface;
use AutoposterBundle\Validator\Constraints\Hash;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Validator\Validator\RecursiveValidator;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\Post("/autopost")
     */
    public function getDefaultsAction(Request $request)
    {
        $body = $request->getContent();

        /** @var Serializer $serializer */
        $serializer     = $this->get('jms_serializer');
        /** @var Autopost $autoPostObject */
        try {
            $autoPostObject = $serializer->deserialize($body, Autopost::class, 'json');
        } catch ( RuntimeException $ex) {
            return [
                'result'    => 'NOK',
                'recordid'  => null,
                'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
                'hash'      => null,
                'errorcode' => 'Invalid JSON provided',
            ];
        }

        $hashConstraint = new Hash();
        /** @var RecursiveValidator $validator */
        $validator      = $this->get('validator');
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors         = $validator->validate($autoPostObject, $hashConstraint);

        if ($errors->count()) {

            return [
                'result'    => 'NOK',
                'recordid'  => null,
                'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
                'hash'      => $autoPostObject->getHash(),
                'errorcode' => $errors[0]->getMessage(),
            ];
        }

        /** @var AutopostSaverInterface $autopostSaver */
        $autopostSaver  = $this->get('AutopostSaverService');
        $autopostSaver->save($autoPostObject);

        return [
            'result'    => 'OK',
            'recordid'  => $autoPostObject->getId(),
            'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
            'hash'      => $autoPostObject->getHash(),
            'errorcode' => null,
        ];
    }

    /**
     * @Rest\Post("/revoke")
     */
    public function revokeAction(Request $request)
    {
        $body = $request->getContent();
        /** @var Serializer $serializer */
        $serializer     = $this->get('jms_serializer');
        /** @var Revoke $revokeObject */
        try {
            $revokeObject = $serializer->deserialize($body, Revoke::class, 'json');
        } catch ( RuntimeException $ex) {
            return [
                'result'    => 'NOK',
                'recordid'  => null,
                'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
                'hash'      => null,
                'errorcode' => 'Invalid JSON provided',
            ];
        }

        /** @var AutopostSaverInterface $autopostSaver */
        $autopostSaver  = $this->get('AutopostSaverService');
        $result         = $autopostSaver->revoke($revokeObject);

        if ($result === true) {
            return [
                'result'    => 'OK',
                'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
                'hash'      => $revokeObject->getHash(),
                'errorcode' => null,
            ];
        }

        return [
            'result'    => 'NOK',
            'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
            'hash'      => $revokeObject->getHash(),
            'errorcode' => $result,
        ];
    }
}
