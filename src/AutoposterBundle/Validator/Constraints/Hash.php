<?php

namespace AutoposterBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Hash extends Constraint
{
    public $message = 'The hash {{ hash }} is invalid';
}