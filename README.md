Create a blank project

```
composer create-project symfony/framework-standard-edition autoposter 3.4.*
```

add the following repository to the composer.json

```
"repositories": [
    {
        "type": "git",
        "url": "https://stefanvandekaa@bitbucket.org/byggblock/autoposter-bundle.git"
    }
],
```

add the following requirement

```
"byggblock/autoposter-bundle": "dev-master"
```

run the following command in the project folder

```  
composer update 
```

add the bundles to app/AppKernel.php

```
new \AutoposterBundle\AutoposterBundle(),
new FOS\RestBundle\FOSRestBundle(),
new JMS\SerializerBundle\JMSSerializerBundle(),
```

add the following routes to app/config/routing.yml

```
autoposter-bundle:
    resource: '@AutoposterBundle/Resources/config/routing.yml'
```

replace the contents of app/config/security.yml with the following:

```
# To get started with security, check out the documentation:
# https://symfony.com/doc/current/security.html
security:
    encoders:
        Symfony\Component\Security\Core\User\User: plaintext

    providers:
        api_provider:
            memory:
                users:
                    '%basic_auth_username%': { password: '%basic_auth_password%', roles: 'ROLE_API_USER' }

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        api:
            pattern:  ^/api/
            provider: api_provider
            security: true
            http_basic:
                realm: "Api"
        main:
            anonymous: ~
```

add the following parameters to app/config/parameters.yml.dist and also copy them into app/config/parameters.yml for actual use.
This file is ignored by git and therefor the dist files needs to have to know these values are required.

```
parameters:
    hash_secret: 'hash secret as provided by ByggBlock'

    basic_auth_username: 'basicauth username'
    basic_auth_password: 'basicauth_password'

    autopost_service: 'AutoposterBundle\Service\AutopostSaverService' # default class, can be overridden.
    autopost_tablename: 'autopost' # default table name to write data in.
    autopost_tablename_revoke: 'revoke_records' # default table name to write data in.
    autopost_mapping: [] # default mapping, can be overridden if fields have different names.
    autopost_mapping_revoke: [] # default mapping, can be overridden if fields have different names.
```

add the following content to the bottom of app/config/config.yml

```
fos_rest:
    body_listener: true
    format_listener:
        rules:
            - { path: '^/', priorities: ['json'], fallback_format: json, prefer_extension: false }
    param_fetcher_listener: true
    view:
        view_response_listener: 'force'
        force_redirects:
            html: true
```

if the default database will be used, the following command needs to be run in order to generate the correct table

```
php bin/console doctrine:schema:create
```

The application should be working now. We can help you with testing the setup.