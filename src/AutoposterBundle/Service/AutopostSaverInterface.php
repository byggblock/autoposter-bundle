<?php

namespace AutoposterBundle\Service;


use AutoposterBundle\Entity\Autopost;
use AutoposterBundle\Entity\Revoke;
use Doctrine\Bundle\DoctrineBundle\Registry;

interface AutopostSaverInterface
{
    /**
     * AutopostSaverService constructor.
     *
     * @param Registry  $doctrine
     * @param string    $tableName
     * @param array     $mapping
     * @param string    $tableNameRevoke
     * @param array     $mappingRevoke
     */
    public function __construct(Registry $doctrine, $tableName, $mapping, $tableNameRevoke, $mappingRevoke);

    /**
     * Save the Autopost object to the database.
     *
     * @param Autopost $record
     *
     * @return boolean
     */
    public function save(Autopost &$record);

    /**
     * Revoke the Autopost object to the database.
     *
     * @param Revoke $record
     *
     * @return boolean|string true or the error message
     */
    public function revoke(Revoke &$record);
}