<?php

namespace AutoposterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('autoposter');
        $rootNode->children()
                    ->scalarNode('hash_secret')->end()
                    ->scalarNode('basic_auth_username')->end()
                    ->scalarNode('basic_auth_password')->end()
                    ->scalarNode('autopost_service')->defaultValue('AutoposterBundle\Service\AutopostSaverService')->end()
                    ->scalarNode('autopost_tablename')->defaultValue('autopost')->end()
                    ->arrayNode('autopost_mapping')->end()
                ->end();

        return $treeBuilder;
    }
}