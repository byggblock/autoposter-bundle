<?php

namespace AutoposterBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Revoke
 * @package AutoposterBundle\Entity
 *
 * @ORM\Table(name="revoke_records")
 * @ORM\Entity
 */
class Revoke
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("date")
     *
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("hash")
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    protected $hash;

    /**
     * @JMS\Type("string")
     * @JMS\SerializedName("reason")
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=true)
     */
    protected $reason;

    /**
     * @ORM\ManyToOne(targetEntity="Autopost", inversedBy="revokes")
     * @ORM\JoinColumn(name="autopost_id", referencedColumnName="id")
     */
    protected $autopost;

    public function __construct()
    {
        $this->setDate(new \DateTime());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        if (! $this->date) {
            $this->setDate(new \DateTime());
        }

        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param mixed $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return mixed
     */
    public function getAutopost()
    {
        return $this->autopost;
    }

    /**
     * @param mixed $autopost
     */
    public function setAutopost($autopost)
    {
        $this->autopost = $autopost;
    }
}